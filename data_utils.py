import numpy as np
import random
import pickle
import requests
import json
from ast import literal_eval
#from pycorenlp import StanfordCoreNLP
from multiprocessing.dummy import Pool as ThreadPool
from tqdm import tqdm
from multiprocessing import cpu_count
import logging
import os
from itertools import chain
from torch.utils.data import Dataset, DataLoader
import pandas as pd
import pickle
from functools import partial
from keras.preprocessing.sequence import pad_sequences


#n_cores = 100
#nlp = StanfordCoreNLP('http://localhost:9000')

def setup_logger(logger_name, log_file, level=logging.INFO):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s : %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler)

def lcs(a, b):
    if len(a) < len(b):
        a, b = b, a
    if len(b) == 0:
        return 0
    mat = [[0 for col in range(len(b) + 1)] for row in range(len(a) + 1)]
    mat_bkp = [[0 for col in range(len(b) + 1)] for row in range(len(a) + 1)]
    for i, ai in enumerate(a):
        for j, bj in enumerate(b):
            if ai == bj:
                value = mat[i][j] + 1
                mat_bkp[i + 1][j + 1] = 1
            else:
                value = max(mat[i + 1][j], mat[i][j + 1])
                if mat[i + 1][j] < mat[i][j + 1]:
                    mat_bkp[i + 1][j + 1] = 2
                else:
                    mat_bkp[i + 1][j + 1] = 3
            mat[i + 1][j + 1] = value
    return mat, mat_bkp


def find_span(a, b, beta):
    mat, mat_bkp = lcs(a, b)
    i = len(mat_bkp) - 1
    j = len(mat_bkp[0]) - 1
    ln = mat[i][j]
    score = ln / len(a)
    if score < beta:
        return -1, -1

    rtn_beg_span = -1
    rtn_end_span = -1
    rtn_len = 100000
    for i in range(len(mat_bkp) - 1, 0, -1):
        if(mat[i][j] == ln):
            beg_span, end_span = fast_span(mat_bkp, mat, i, j)
            #print(beg_span, end_span)
            if((end_span - beg_span) < rtn_len):
                rtn_len = end_span - beg_span
                rtn_beg_span = beg_span
                rtn_end_span = end_span
    return [rtn_beg_span, rtn_end_span, score]


def fast_span(mat_bkp, mat, i, j):
    begSpan = -1
    endSpan = -1
    while(1):
        if (i == 0 or j == 0):
            return begSpan, endSpan

        if (mat_bkp[i][j] == 1):
            if endSpan == -1:
                endSpan = i
            i = i - 1
            j = j - 1
            #print('move diag', i, j)
            if mat[i][j] == 0:
                begSpan = i + 1
                return begSpan, endSpan

        elif mat_bkp[i][j] == 2:
            i = i - 1
            #print('move up', i, j)

        elif mat_bkp[i][j] == 3:
            j = j - 1


find_span([2, 0, 1], [1, 0, 1], 0)

pbar = None


def get_glove(token):
    vec = pickle.loads(
        requests.post("http://10.150.40.20:8001/getvec", data=json.dumps({"token": token})).content)
    if vec is None:
        return np.zeros(300)
    return vec


def get_vec(vocab, vector_type):
    '''collects vectors of all the words in vocab and returns them as dict {word: vector}'''
    global pbar
    pbar = tqdm(total=len(vocab))
    vocab = list(vocab)

    if vector_type == "glove":
        results = dict()
        for v in vocab:
            results[v] = get_glove(v)
        return results

    elif vector_type == "agi":
        pool = ThreadPool(n_cores)
        results = pool.map(get_agi, vocab)
        pool.close()
        pool.join()
        results_new = dict()
        for i in results:
            temp = list(i.keys())[0]
            results_new[temp] = i[temp]
        results = results_new
    return results


def tokenize(sent):
    output = nlp.annotate(sent, properties={
        'annotators': 'tokenize,ssplit',
        'outputFormat': 'json'
    })
    tokens = []
    if isinstance(output, str):
        output = ast.literal_eval(output)
    for i in output['sentences']:
        tokens.extend([x['originalText'] for x in i['tokens']])
    return tokens


def load_preprocessed_data(type):
    data = pickle.load(
        open("/data/ramitra/marco/preprocessed/gmc/{0}/data.pkl".format(type), "rb"))
    i2w = pickle.load(
        open("/data/ramitra/marco/preprocessed/gmc/index2word.pkl", "rb"))
    vec = np.array(pickle.load(
        open("/data/ramitra/marco/preprocessed/gmc/vectors.pkl", "rb")))
    print(len(data), len(i2w), vec.shape)

    # idx = np.random.randint(len(data))
    # print(" ".join([i2w[x] for x in data[idx]["p"]]) + "\n")
    # print(" ".join([i2w[x] for x in data[idx]["q"]]) + "\n")
    # print(" ".join([i2w[x] for x in data[idx]["a"]]) + "\n")

    return data, i2w, vec

def write_file(x):
    logging.info(x)

def pad_jagged_array(x, max_len, pad_value=-1):
    # return pad_sequences(sequences=x, maxlen=max_len, dtype="int32", padding="post", value=pad_value)
    if x.ndim == 2: # already not jagged
        return x
    x_ = [j + [pad_value] * (max_len - len(j)) for j in x]
    return x_

def get_batch(data, size, hp):
    batch = np.random.choice(data, size, replace=False)
    batch_p = np.array([x['p'] for x in batch])
    batch_q = np.array([x['q'] for x in batch])
    batch_a = np.array([x['a'] for x in batch])
    batch_d_out = np.array([x[1:] for x in batch_a])
    batch_d_in = np.array([x[:-1] for x in batch_a])
    batch_lp = np.array([x["l_p"] for x in batch])
    batch_lq = np.array([x["l_q"] for x in batch])
    batch_la = np.array([x["l_a"] - 1 for x in batch])
    # batch_id = np.array([x["id"] for x in batch])
    # print(max(batch_lp), max(batch_lq))
    batch_p = pad_jagged_array(batch_p, max(batch_lp), hp.PAD)
    batch_q = pad_jagged_array(batch_q, max(batch_lq), hp.PAD)
    batch_d_out = pad_jagged_array(batch_d_out, max(batch_la), hp.PAD)
    batch_d_in = pad_jagged_array(batch_d_in, max(batch_la), hp.PAD)

    return {
        "p": batch_p,
        "q": batch_q,
        "d_out": batch_d_out,
        "d_in": batch_d_in,
        "p_len": batch_lp,
        "q_len": batch_lq,
        "d_len": batch_la,
        }
        
# def get_logger(filename, mode='w'):
#     logger = logging.getLogger('aqa')
#     hdlr = logging.FileHandler(filename, mode)
#     formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
#     hdlr.setFormatter(formatter)
#     logger.addHandler(hdlr)
#     logger.setLevel(logging.INFO)
#     return logger

def get_batch_joint(data, size, hp):
    batch = random.sample(data, size)
    batch_lp = np.array([[x['l_p'] for x in y] for y in batch])
    batch_lq = np.array([[x['l_q'] for x in y] for y in batch])
    # print(batch_lq)
    # input(batch_lq.shape)
    # input()

    batch_c = np.array([[x['c'] for x in y] for y in batch])

    batch_p = np.array([[x['p'] for x in y] for y in batch])
    batch_p = np.array([pad_jagged_array(x, hp.max_p_len) for x in batch_p])

    batch_q = np.array([[x['q'] for x in y] for y in batch])
    batch_q = np.array([pad_jagged_array(x, np.max(batch_lq)) for x in batch_q])

    return {
        "p": batch_p,
        "q": batch_q,
        "p_len": batch_lp,
        "q_len": batch_lq,
        "c": batch_c
        }

def pad_item(item, field, value):
    query = [x[field] for x in item]
    maxlen = max([len(x) for x in query])
    query = pad_jagged_array(np.array(query), maxlen, value)
    return query

def pad_batch(value, sample):

    # query_, q_len_, passage_, p_len_ = [], [], [], []
    # for x in sample:
    #     query_.extend(x["query_"])
    #     passage_.extend(x["passage"])

    # for x, y in zip(query_, passage_):
    #     q_len_.append(len(x))
    #     p_len_.append(len(y))
    max_p_len = max([max([len(i) for i in x["passage"]]) for x in sample])

    return {
                        "query": pad_item(sample, "query", value), 
                        "passage": [pad_jagged_array(np.array(x["passage"]), max_p_len, value) for x in sample],
                        # "passage_": pad_sequences(passage_, maxlen=max(p_len_), value=value, padding="post"),
                        # "query_": pad_sequences(query_, maxlen=max(q_len_), value=value, padding="post"),
                        # "q_len_": np.array(q_len_),
                        # "p_len_": np.array(p_len_),
                        "ans_out": pad_item(sample, "ans_out", value), 
                        "q_len": [x["q_len"] for x in sample], 
                        "p_len": [x["p_len"] for x in sample],
                        "is_selected": [x["is_selected"] for x in sample],
                        "query_id": [x["query_id"] for x in sample],
                        "a_len": [x["a_len"] for x in sample]
            }

class MyDataset(Dataset):

    def __init__(self, root, hp, is_test=False):
        self.hp = hp

        self.id2query = pd.read_csv(os.path.join(root, "id2query.tsv"), sep="\t", header=None, names=["query_id", "num_passages", "query"] if not is_test else ["query_id", "query"])
        self.query2psg = pd.read_csv(os.path.join(root, "query2psg.tsv"), sep="\t", header=None, names=["query_id", "is_selected", "rank", "passage"])

        self.query2psg["passage"] = self.query2psg["passage"].apply(lambda x: list(map(int, str(x).split())))
        self.id2query["query"] = self.id2query["query"].apply(lambda x: list(map(int, str(x).split())))

        # if not is_test:
        self.query2ans = pd.read_csv(os.path.join(root, "query2ans.tsv"), sep="\t", header=None, names=["query_id", "answer"])
        self.query2ans["answer"] = self.query2ans["answer"].apply(lambda x: list(map(int, str(x).split())))


        # remove queries (not having ten passages & not having at least one selected passage)

        grouped_data = self.query2psg.groupby(["query_id"]).agg({"passage": lambda x: len(x), 
                                                                          "is_selected": lambda x: sum(x)})
        good_ids = grouped_data.loc[(grouped_data["passage"] == 10) & (grouped_data["is_selected"] > 0)].drop(["passage", "is_selected"], axis=1)
        self.id2query = self.id2query.join(good_ids, on="query_id", how="inner")
        self.query2psg = self.query2psg.join(good_ids, on="query_id", how="inner")
        self.query2ans = self.query2ans.join(good_ids, on="query_id", how="inner")
        # num_passages = dict(self.query2psg.groupby(["query_id"], as_index=False).size())
        # good_ids = [k for k,v in num_passages.items() if v == 10]
        # self.id2query = self.id2query.loc[self.id2query["query_id"].isin(good_ids)]
        # self.query2psg = self.query2psg.loc[self.query2psg["query_id"].isin(good_ids)]
        # self.query2ans = self.query2ans.loc[self.query2ans["query_id"].isin(good_ids)]


    def __getitem__(self, idx):
        query = self.id2query.iloc[idx]
        query_id = query["query_id"]
        passage = self.query2psg[self.query2psg["query_id"]==query["query_id"]]
        answer = self.query2ans[self.query2ans["query_id"]==query["query_id"]]
        
        is_selected = passage["is_selected"].values
        is_selected = np.nonzero(is_selected)[0][0]

        passage = list(passage["passage"].values)
        answer = list(answer["answer"].values)[0]
        query = query["query"]

        p_len = [len(x) for x in passage]
        q_len = len(query)
        a_len = len(answer)
        ans_in = [self.hp.BOS] + answer[:-1]
        ans_out = answer[1:] + [self.hp.EOS]

        query_repeat = np.repeat([query], len(passage), 0)

        return { "query": query, "passage": passage, "ans_in": ans_in, "ans_out": ans_out, "q_len": q_len, "p_len": p_len , "query_": query_repeat,
                    "a_len": a_len, "is_selected": is_selected, "query_id": query_id }

    def __len__(self):
        return self.id2query.shape[0]

# if __name__ == "__main__":

    # vec = pd.read_csv(os.path.join("/data/ramitra/marco/", "vec.tsv"), sep="\t", header=None, names=["id", "vector"])
    # print(vec.head())
    # vec = np.array(list(vec["vector"].apply(lambda x: list(map(float, x.split(',')))).values))
    # print(vec.shape)
    # PAD = vec.shape[0] - 1
    # PAD=5

    # dev_data = MyDataset("/data/ramitra/marco/dev")
    # dev_loader = DataLoader(dev_data, batch_size=2, collate_fn=partial(pad_batch, PAD), 
    #                                                                 shuffle=True)


    # for idx, batch in enumerate(dev_loader):
    #     print(1)
    # print(dev_data.vec.shape)
