from model import *
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from data_utils import load_preprocessed_data, write_file, get_batch, setup_logger, MyDataset, pad_batch
from torch.utils.data import DataLoader
import argparse
import numpy as np
import torch.optim as optim
from tqdm import trange, tqdm
import sys
import os
import pandas as pd
from functools import partial
#import visdom
from nlgeval import Rouge

rg = Rouge()

# vis = visdom.Visdom()


vec = pd.read_csv(os.path.join("/data/ramitra/marco/", "vec.tsv"), sep="\t", header=None, names=["id", "vector"])
vec = np.array(list(vec["vector"].apply(lambda x: list(map(float, x.split(',')))).values))

vec = np.r_[vec, np.zeros((1, 300))]
PAD = vec.shape[0] - 1

vec = np.r_[vec, np.random.randn(1, 300)]
BOS = vec.shape[0] - 1

vec = np.r_[vec, np.random.randn(1, 300)]
EOS = vec.shape[0] - 1

rg = Rouge()

def convert2str(x):
    res = []    
    for i in x:        
        temp = []
        for j in i:
            if j not in i2w:
                continue
            if j == -1 or i2w[j] == "<eos>":
                break
            temp.append(i2w[j])
        res.append(" ".join(temp))
    return res

parser = argparse.ArgumentParser()
parser.add_argument("--w_dim", type=int, default=300)
parser.add_argument("--h_dim", type=int, default=256)
parser.add_argument("--batch_size", type=int, default=40)
parser.add_argument("--learning_rate", type=float, default=1e-1)
parser.add_argument("--max_d_len", type=int, default=51)
parser.add_argument("--max_p_len", type=int, default=200)
parser.add_argument("--dropout", type=float, default=0.2)
parser.add_argument("--teacher_forcing", action="store_true")
parser.add_argument("--no_attention", action="store_true")
parser.add_argument("--resume", action="store_true")
parser.add_argument("--vocab_size", type=int, default=len(vec))
parser.add_argument("--step", type=int, default=50)
parser.add_argument("--BOS", type=int, default=BOS)
parser.add_argument("--EOS", type=int, default=EOS)
parser.add_argument("--PAD", type=int, default=PAD)
parser.add_argument("--vec", default=vec)
parser.add_argument("--model_path", type=str, default="model")
parser.add_argument("--mode", choices=["train","infer", "vis"], default="train")
parser.add_argument("--max_iterations", type=int, default=51)
parser.add_argument("--no_save", action="store_true")
parser.add_argument("--cov", action="store_true")
parser.add_argument("--pg", action="store_true")
parser.add_argument("--num_layers", type=int, default=1)
parser.add_argument("--device", default=T.device("cuda:0"))

params = parser.parse_args()

train_data = MyDataset("/data/ramitra/marco/train", params)
dev_data = MyDataset("/data/ramitra/marco/dev", params)

# print(params.BOS, params.EOS)

train_loader = DataLoader(train_data, batch_size=params.batch_size, collate_fn=partial(pad_batch, params.PAD))
dev_loader = DataLoader(dev_data, batch_size=params.batch_size, collate_fn=partial(pad_batch, params.PAD),
                                                                    shuffle=True)

model = QAModel(params)
model = model.to(params.device)
avg_loss = 0

def visualize_attention(affinity, x_tokens, y_tokens, xlabel, ylabel, title=None, filename="vis/attention.png"):
    fig, ax = plt.subplots(figsize=(len(x_tokens), len(y_tokens)))
    ax.set_title(title)
    sns.heatmap(affinity, cmap="Blues")
    ax.set_xticklabels(x_tokens)
    ax.set_yticklabels(y_tokens)
    ax.set(xlabel=xlabel, ylabel=ylabel)
    plt.savefig(filename)


def loss_fn(logprobs, target, dl):
    criterion = nn.NLLLoss(ignore_index=params.PAD)
    # loss = criterion(logprobs.view(-1, params.n_classes), target.view(-1))
    gold_log_probs = T.gather(logprobs, -1, target.unsqueeze(-1)).squeeze()
    loss_mask = T.ne(target, params.PAD).float()
    masked_probs = gold_log_probs * loss_mask
    loss =  - T.sum(masked_probs, -1) / dl.float()
    return loss

def inference(model, data):
    model.eval()
    preds, tars, losses = [], [], []
    for i in trange(0, len(data), params.batch_size):
        if i + params.batch_size > len(data):
            continue
        p, t, l = evaluation(model, data[i: i+params.batch_size])
        preds.extend(p)
        tars.extend(t)
        losses.extend(l)

    res = compute_batch_metrics(convert2str(preds), convert2str(tars))
    return {"rouge-l": res, "loss": np.mean(losses)}




def compute_score(target, preds):
    target = " ".join(map(str, target))
    preds = " ".join(map(str, preds))
    return rg.calc_score([target], [preds])

def is_passage_correclty_selected(preds, target):    
    ''' 
    returns True if at least one of the K correct reference passages is present
    in the top K predicted passages, false otherwise.
    '''
    # preds = T.sigmoid(preds)
    # print(preds)
    # print(target)
    # input()
    top_psgs_target = target.nonzero().view(-1).detach().cpu().numpy()
    num_correct = target.sum().type(T.LongTensor)
    top_psgs_preds = preds.sort(descending=True)[1][:num_correct].detach().cpu().numpy()
    is_correct = set(top_psgs_target).intersection(top_psgs_preds)
    return bool(is_correct)

def evaluation():
    model.eval()
    avg_sel_score = []
    for step, batch in enumerate(tqdm(train_loader)):

        d_out = T.LongTensor(batch["ans_out"]).to(params.device)
        dl = T.LongTensor(batch["a_len"]).to(params.device)

        q = T.LongTensor(batch["query"]).to(params.device)
        p = T.LongTensor(batch["passage"]).to(params.device)
        p_len = T.LongTensor(batch["p_len"]).to(params.device)
        is_selected = T.LongTensor(batch["is_selected"]).to(params.device)

        q_len = T.LongTensor(batch["q_len"]).to(params.device)
        a_len = batch["a_len"]

        _, conf = model(q, p, q_len, p_len, max(a_len))
        sel_loss = selection_criterion(conf, is_selected)
        avg_sel_score.extend([is_passage_correclty_selected(x, y) for x,y in zip(conf, is_selected)])
        # corrects = []
        # sel_loss = []
        # for i, p_ in enumerate(p):  # iterating over batch
        #     q_ = q[i].unsqueeze(0).repeat(p_.shape[0], 1)
        #     q_len_ = q_len[i].repeat(p_.shape[0])
        #     p_len_ = p_len[i]

        #     q_ = q_[:,:q_len_[0]]
        #     _, conf = model(q_, p_, q_len_, p_len_, max(a_len))

        #     corrects.append(is_passage_correclty_selected(conf, is_selected[i]))
        #     sel_loss.append(selection_criterion(conf.view(1, -1), is_selected[i].view(1, -1)).item())

        # score = np.mean([compute_score(tars[i], preds[i]) for i in range(len(preds))])
        if step == 500:
            break

    avg_sel_score = np.mean(avg_sel_score)
    model.train()
    return {"loss": sel_loss, "sel_score": avg_sel_score}
    #     input()
    # model.train()
    # return { "dev_rouge": np.mean(score)}
    

# selection_criterion = nn.MultiLabelSoftMarginLoss()
def selection_criterion(preds, targets):
    gold_psg_log_probs = T.log(T.gather(preds, -1, targets.unsqueeze(-1)))
    loss = - gold_psg_log_probs.mean()
    return loss


optimizer = optim.Adadelta([x for x in model.parameters() if x.requires_grad == True], lr=params.learning_rate)
alpha = 0.5

if params.resume:
    print("resuming")
    checkpoint = T.load("/data/ramitra/marco/saves/qa.pth")
    # items_not_in_checkpoint = {k:v for k,v in model.state_dict().items() if k not in checkpoint["state_dict"]}    
    # checkpoint["state_dict"].update(items_not_in_checkpoint)
    model.load_state_dict(checkpoint["state_dict"])
    optimizer.load_state_dict(checkpoint["optimizer"])

print(evaluation())
input()

try:
    if params.mode == "train":
        tr = trange(10)
        best_rouge = 0
        avg_loss = 0
        avg_decode_loss = 0
        avg_sel_loss = []
        avg_sel_score = []
        avg_rouge_score = 0
        # win = vis.line(Y=np.random.randn(10), X=np.random.randn(10))
        for i in tr:
            for step, batch in enumerate(tqdm(train_loader)):
                optimizer.zero_grad()

                # print([len(x) for x in batch["passage"]])

                d_out = T.LongTensor(batch["ans_out"]).to(params.device)
                dl = T.LongTensor(batch["a_len"]).to(params.device)

                q = T.LongTensor(batch["query"]).to(params.device)
                p = T.LongTensor(batch["passage"]).to(params.device)
                p_len = T.LongTensor(batch["p_len"]).to(params.device)
                is_selected = T.LongTensor(batch["is_selected"]).to(params.device)

                q_len = T.LongTensor(batch["q_len"]).to(params.device)
                a_len = batch["a_len"]

                _, conf = model(q, p, q_len, p_len, max(a_len))
                sel_loss = selection_criterion(conf, is_selected)

                avg_sel_loss.append(sel_loss.item())
                avg_sel_score.extend([is_passage_correclty_selected(x, y) for x,y in zip(conf, is_selected)])

                # decode_loss = []
                # score = []
                # sel_score = []
                # sel_loss = []

                # for i, p_ in enumerate(p):  # iterating over batch
                #     q_ = q[i].unsqueeze(0).repeat(p_.shape[0], 1)
                #     q_len_ = q_len[i].repeat(p_.shape[0])
                #     p_len_ = p_len[i]

                #     q_ = q_[:,:q_len_[0]]
                #     _, conf = model(q_, p_, q_len_, p_len_, max(a_len))
                #     # print(logprobs.shape, p_.shape)
                #     # print(logprobs.shape, p_.shape)
                #     # is_correct = is_passage_correclty_selected(conf, is_selected[i])
                #     # sel_score.append(is_correct)
                    
                #     # avg_sel_score.append(is_correct)
                #     sel_loss.append(selection_criterion(conf.view(1, -1), is_selected[i].view(1, -1)))           
                #     # score.append(compute_score(d_out[i][:dl[i]], logprobs.max(-1)[1][:dl[i]]))
                #     # decode_loss.append(loss_fn(logprobs, d_out[i], dl[i]))

                sel_loss.backward()
                optimizer.step()

                if step % params.step == 0 and step != 0:
                    print("avg selection loss = {}".format(np.mean(avg_sel_loss)))
                    # print("avg selection score = {}".format(np.mean(avg_sel_score)))
                    print("avg selection score = {}".format(evaluation()))
                    avg_decode_loss = avg_sel_loss = avg_score = 0
                    avg_sel_loss = []
                    avg_sel_score = []
                    T.save({
                        "state_dict": model.state_dict(),
                        "optimizer": optimizer.state_dict()
                    }, "/data/ramitra/marco/saves/qa.pth")
                    # print(evaluation())

    elif params.mode == "infer":
        print("inference")
        print(inference(model, valid_data))
    elif params.mode == "vis":
        _ = evaluation(model, valid_data, True)
except KeyboardInterrupt:
    print("User interrupt")
 
