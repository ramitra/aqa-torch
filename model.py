import torch as T
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
from torch.nn.utils.rnn import pad_packed_sequence, pack_padded_sequence

class Memory(nn.Module):
    ''' A memory block to update with information as model reads a passage. This should allow model to
    aggregate information from multiple passages. '''

    def __init__(self, hidden_size):
        super(Memory, self).__init__()

    def forward(self):
        raise NotImplementedError("TODO")

class RNN(nn.Module):
    """ RNN that handles padded sequences """

    def __init__(self, input_size, hidden_size, bidirectional=False):
        super(RNN, self).__init__()
        self.hidden_size = hidden_size
        self.bidirectional = bidirectional
        self.gru = nn.GRU(input_size, hidden_size, batch_first=True, bidirectional=bidirectional)

    def forward(self, seq, seq_length):
        seq_length, idx_sort = T.sort(seq_length, 0, descending=True)
        _, idx_unsort = idx_sort.sort(0)
        seq = seq[idx_sort]
        seq = pack_padded_sequence(seq, seq_length.data.cpu().numpy(), batch_first=True)
        o, s = self.gru(seq)
        # s = s.squeeze()
        if self.bidirectional:
            s = T.transpose(s, 1, 0)
        o, _ = pad_packed_sequence(o, batch_first=True)
        # print(o.shape, seq_length)
        o = o[idx_unsort]
        s = s[idx_unsort]
        if self.bidirectional:
            s = s.view(-1, 2 * self.hidden_size)
        return o, s

class Point_Gen(nn.Module):

    def __init__(self):
        super(Point_Gen, self).__init__()
        self.weights = nn.ParameterList([nn.Parameter(T.randn(1, 512)), nn.Parameter(T.randn(1, 300)), nn.Parameter(T.randn(1, 512))])
        self.bias = nn.Parameter(T.randn(1))

    def forward(self, *args):
        res = 0
        for i, arg in enumerate(args):
            res += F.linear(arg, self.weights[i]) #.cuda()
        res += self.bias
        p_gen = T.sigmoid(res)
        return p_gen
        
class PassageAggregator(nn.Module):
    
    def __init__(self, input_dim, hidden_dim):
        super(PassageAggregator, self).__init__()
        self.hidden_dim = hidden_dim
        self.passage_scorer = nn.Sequential(*[nn.Linear(input_dim, hidden_dim),
                                            nn.Linear(hidden_dim, hidden_dim),
                                            nn.Linear(hidden_dim, 1)])

    def forward(self, passages):
        ''' passages: [batch_size, num_passages, input_dim]'''

        weights = F.softmax(self.passage_scorer(passages).squeeze(-1), dim=-1).unsqueeze(-1) # [batch_size, num_passages, 1]
        # weights = self.passage_scorer(passages)
        agg_psg_repr = T.bmm(passages.transpose(2, 1), weights).squeeze(-1) # [batch_size, input_dim]

        return agg_psg_repr, weights.squeeze(-1)

class ScaledDotProductAttention(nn.Module):

    def __init__(self, input_dim, hidden_dim, dropout=0.):
        super(ScaledDotProductAttention, self).__init__()
        self.hidden_dim = hidden_dim
        self.linear1 = nn.Linear(input_dim, hidden_dim, bias=False)
        self.linear2 = nn.Linear(input_dim, hidden_dim, bias=False)
        self.linear3 = nn.Linear(4*hidden_dim, 4*hidden_dim, bias=False)
        self.rnn = RNN(4 * self.hidden_dim, self.hidden_dim, bidirectional=True)
        self.dropout = dropout

    def forward(self, sequence1, sequence2, sequence1_mask, sequence2_length):
        sequence1_ = F.relu(self.linear1(sequence1))
        sequence2_  = F.relu(self.linear2(sequence2))
        output = T.bmm(sequence2_, sequence1_.transpose(2, 1)) / (self.hidden_dim ** 0.5)
        mask = sequence1_mask.unsqueeze(1).repeat(1, output.size()[1], 1)
        output.data.masked_fill_(mask.data, -float("inf"))
        self.weights = F.softmax(output, dim=-1) # [num_examples, psg_len, query_len]
        output = T.bmm(self.weights, sequence1)
        res = T.cat([sequence2, output], dim=-1)

        g = T.sigmoid(self.linear3(res))
        res = res * g
        res = F.dropout(res, self.dropout, training=self.training)
        o, s = self.rnn(res, sequence2_length)
        return o, s

# class Match(nn.Module):
#     """ Matches one sequence with another """

#     def __init__(self, hidden_dim):
#         super(Match, self).__init__()
#         self.hidden_dim = hidden_dim
#         self.linear1 = nn.Linear(2 * self.hidden_dim, self.hidden_dim, bias=False)
#         self.linear2 = nn.Linear(2 * self.hidden_dim, self.hidden_dim, bias=False)
#         self.linear3 = nn.Linear(self.hidden_dim, 1, bias=False)

#     def forward(self, sequence1, sequence2):
#         # return sequence2
#         seq1 = self.linear1(sequence1)
#         seq2 = self.linear2(sequence2)
#         # alignment = F.softmax(T.bmm(seq1, seq2.transpose(1, 2)).transpose(1, 2), dim=-1)
#         # print(alignment.size(), seq1.size())
#         # input()
#         # seq1_aware_seq2 = T.bmm(alignment, seq1)

#         seq1_aware_seq2 = []
#         for i in range(seq2.size()[1]):
#             u_p = seq2[:,i,:]
#             S = []
#             for j in range(seq1.size()[1]):
#                 u_q = seq1[:,j,:]
#                 s = T.tanh(u_p + u_q)
#                 S.append(s)
#             S = T.stack(S, 1).max(1)[0]
#             score = T.sigmoid(self.linear3(S))
#             weighted_p = u_p * score

#         #     S = F.softmax(T.stack(S, 1).squeeze(), dim=-1).unsqueeze(-1)
#         #     c = T.bmm(S.transpose(1, 2), seq1).squeeze()
#             # seq1_aware_seq2.append(T.cat((c, u_p), -1))
#             seq1_aware_seq2.append(weighted_p)
#         seq1_aware_seq2 = T.stack(seq1_aware_seq2, 1)
#         return seq1_aware_seq2

# class VanillaEncoder(nn.Module):
#     """ Encodes a sequence with a RNN """

#     def __init__(self, w_dim, h_dim, n_layers=1):
#         super(VanillaEncoder, self).__init__()
#         self.rnn = RNN(w_dim, h_dim, bidirectional=False)

#     def forward(self, sequence, sequence_length):
#         o, h = self.rnn(sequence, sequence_length)
#         return o, h

class Encoder(nn.Module):
    """ Models relationship between Q and P and encodes them into a vector """

    def __init__(self, w_dim, h_dim, dropout=0):
        super(Encoder, self).__init__()
        self.h_dim = h_dim
        self.w_dim = w_dim
        self.context_gru = RNN(w_dim, h_dim, bidirectional=True)
        self.match_layer = ScaledDotProductAttention(2 * h_dim, h_dim)  
        self.aggregation_gru = RNN(h_dim, h_dim)
        self.dropout = dropout

    def forward(self, query, passage, query_length, passage_length, query_mask):
        u_p = F.dropout(self.context_gru(passage, passage_length)[0], self.dropout, training=self.training)
        u_q, u_q_s = self.context_gru(query, query_length)
        u_q = F.dropout(u_q, self.dropout, training=self.training) 
        matched, s = self.match_layer(u_q, u_p, query_mask, passage_length)
        matched = F.dropout(matched, self.dropout, training=self.training)
        s = F.dropout(s, self.dropout, training=self.training)
        return matched, s, u_q_s

class Attention(nn.Module):
    """ Add-on for a decoder by incorporating which a decoder can become an attentive one. """

    def __init__(self, hidden_size):
        super(Attention, self).__init__()
        # self.attn_linear1 = nn.Linear(2 * hidden_size, 1, bias=False)
        self.linear1 = nn.Linear(hidden_size, int(hidden_size/2), bias=False)
        self.linear2 = nn.Linear(hidden_size, int(hidden_size/2), bias=False)
        self.attn_linear2 = nn.Linear(2 * hidden_size, hidden_size, bias=False)
        self.hidden_size = hidden_size
    
    def forward(self, memory, query):
        """ Attends a particular query to the memory states.
            Arguments:
                memory --> [batch_size, seq_length, hidden_size].
                query --> [batch_size, hidden_size]
            Return:
                scores --> [batch_size, seq_length]
        """
        
        memory_ = F.relu(self.linear1(memory))
        query_ = F.relu(self.linear2(query))

        query_  = query_.unsqueeze(-1)
        alignment = T.bmm(memory_, query_).squeeze(-1) / (self.hidden_size * 0.5)
        alignment.data.masked_fill_(T.eq(alignment, 0).data, -float("inf"))
        attn_weights = F.softmax(alignment, dim=-1) # [N, length]
        context = T.bmm(attn_weights.unsqueeze(1), memory).squeeze(1)
        decoder_state = T.tanh(self.attn_linear2(T.cat((context, query), dim=-1)))
        return decoder_state, attn_weights, context

class Decoder(nn.Module):
    """ Decoder that generates text. Operates in either of the two modes: greedy and teacher forced """
    
    def __init__(self, 
                input_size,
                hidden_size, 
                embedding, 
                token, 
                n_classes, 
                teacher_forcing=False, 
                no_attention=False):

        super(Decoder, self).__init__()
        self.embedding = embedding
        self.start_token = token
        self.hidden_size = hidden_size
        self.no_attention = no_attention
        self.output_layer = nn.Linear(hidden_size, n_classes, bias=False)
        self.cell = nn.GRUCell(input_size, hidden_size)
        self.teacher_forcing = teacher_forcing
        self.point_gen = Point_Gen()
        if not no_attention:
            self.attention = Attention(hidden_size)

    def forward(self, decoder_state, 
                      n_decoding_iters, 
                      decoder_in=None,
                      encoder_outputs=None, encoder_input_ids=None):

        logits = []
        batch_size = decoder_state.size()[0]
        inputs_id = Variable(T.LongTensor(batch_size).fill_(self.start_token)).cuda()

        if self.teacher_forcing:
            o, _ = self.temp(decoder_in, decoder_state.unsqueeze(0))
            logits = self.output_layer(o)

        else:
            for _ in range(n_decoding_iters):
                inputs = self.embedding(inputs_id)

                if not self.no_attention:
                    decoder_state, attn_weights, context = self.attention(encoder_outputs, decoder_state)
                p_gen = self.point_gen(decoder_state, inputs, context)
                
                # print("cell before", inputs.shape, decoder_state.shape)
                decoder_state = self.cell(inputs, decoder_state)
                logit = F.softmax(self.output_layer(decoder_state), dim=-1)
                inputs_id = T.topk(logit, 1, dim=-1)[1].squeeze(-1)
                
                scattered_attn_dist = T.zeros_like(logit)
                scattered_attn_dist.scatter_add_(1, encoder_input_ids, attn_weights)
                logit = p_gen * logit + (1 - p_gen) * scattered_attn_dist
                
                logits.append(logit)
                
            logits = T.stack(logits, 1)
        logprobs = T.log(logits)

        return logits, logprobs

class QAModel(nn.Module):

    def __init__(self, params):
        super(QAModel, self).__init__()
        self.params = params
        self.embedding = nn.Embedding(params.vocab_size, params.w_dim)
        self.embedding.weight.data.copy_(T.from_numpy(params.vec))
        self.embedding.weight.requires_grad = False
        self.encoder = Encoder(params.w_dim, params.h_dim, params.dropout)
        # self.encoder1 = Encoder(2 * params.h_dim, params.h_dim, params.dropout)

        # self.decoder = Decoder(params.w_dim, 2 * params.h_dim, self.embedding, params.BOS, 
        #                         params.vocab_size, params.teacher_forcing, params.no_attention)
        self.linear = nn.Linear(4 * params.h_dim, 2 * params.h_dim, bias=False)

        self.psg_agg = PassageAggregator(4*params.h_dim, params.h_dim)

        self.reset_parameters()

    def reset_parameters(self):
        """ initializes weights of the full model """
        for param in self.parameters():
            if not param.requires_grad: # embedding
                continue
            if param.dim() < 2: # bias
                nn.init.constant_(param, 0)
            else:
                nn.init.xavier_uniform_(param)


    def forward(self, query, psg, query_len, psg_len, max_decoding_step=50):
        
        query = query.unsqueeze(1).repeat(1, 10, 1)
        query_len = query_len.unsqueeze(1).repeat(1, 10)
        q_mask = T.eq(query, self.params.PAD)
        q_emb = self.embedding(query)
        p_emb = self.embedding(psg)

        q_mask = q_mask.view(self.params.batch_size * 10, -1)
        q_emb = q_emb.view(self.params.batch_size * 10, -1, 300)
        p_emb = p_emb.view(self.params.batch_size * 10, -1, 300)
        query_len = query_len.view(-1)
        psg_len = psg_len.view(-1)

        # print(q_mask.shape, q_emb.shape, p_emb.shape, query_len.shape, psg_len.shape)

        encoder_outputs, encoder_last_state, question_last_state = self.encoder(q_emb, p_emb, query_len, psg_len, q_mask)
        
        encoder_last_state = encoder_last_state.view(self.params.batch_size, 10, -1)
        question_last_state = question_last_state.view(self.params.batch_size, 10, -1)

        # p_weights = alignment.max(-1)[0].mean(-1)
        agg_p, p_weights = self.psg_agg(T.cat((encoder_last_state, question_last_state), dim=-1))
        # print(encoder_outputs.shape, encoder_last_state.shape, question_last_state.shape, p_weights.shape)

        # _, psg_sel = T.max(p_weights, 0)
        # correct_psg_out = encoder_outputs[psg_sel].unsqueeze(0)
        # correct_last_state = encoder_last_state[psg_sel].unsqueeze(0)
        # question_last_state = question_last_state[0].unsqueeze(0)
        # psg = psg[psg_sel].unsqueeze(0)

        # combined_last_state = T.tanh(self.linear(T.cat([correct_last_state, question_last_state], dim=-1)))
        # logits, logprobs = self.decoder(combined_last_state, max_decoding_step, None, correct_psg_out, psg)

        # conf = logits.max(-1)[0].sum(-1)
        # return logprobs.squeeze(0), p_weights

        return p_weights, p_weights
        # return logprobs[T.topk(conf, k=1, dim=-1)[1]].squeeze(0), conf
