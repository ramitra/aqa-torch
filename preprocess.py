import pickle
from tqdm import tqdm, trange
from itertools import chain
from data_utils import get_glove, find_span
import numpy as np
from collections import Counter


# train_data = pickle.load(
#     open("/data/ramitra/marco/preprocessed/train/tokenized_data.pkl", "rb"))
# dev_data = pickle.load(
#     open("/data/ramitra/marco/preprocessed/dev/tokenized_data.pkl", "rb"))
train_data = pickle.load(open('/data/ramitra/marco/tokenized_train.pkl', 'rb'))
dev_data = pickle.load(open('/data/ramitra/marco/tokenized_dev.pkl', 'rb'))
print(dev_data[0])
input()
# for i in range(len(train_data)):
#     train_data[i]["query"] = list(map(str.lower, train_data[i]["query"]))
#     train_data[i]["pos_p"] = list(map(str.lower, train_data[i]["pos_p"]))
#     train_data[i]["ans"] = list(map(str.lower, train_data[i]["ans"]))
# for i in range(len(dev_data)):
#     dev_data[i]["query"] = list(map(str.lower, dev_data[i]["query"]))
#     dev_data[i]["pos_p"] = list(map(str.lower, dev_data[i]["pos_p"]))
#     dev_data[i]["ans"] = list(map(str.lower, dev_data[i]["ans"]))

print(len(train_data), len(dev_data))
vocab = list()
for data in (train_data, dev_data):
    for i in tqdm(data):
        vocab.extend(i["query"])
        vocab.extend(list(chain(*i["pos_p"])))
        # vocab.extend(list(chain(*i["neg_p"])))
        vocab.extend(list(chain(*i["ans"])))

# vocab = list(set(vocab))
vocab = Counter(vocab).most_common(30000)
vocab = [x[0] for x in vocab]

vec = dict()
glove = pickle.load(open("/data/ramitra/glove.840B.300d.pkl", "rb"))
print("glove no. of words=", len(glove))

oov_count = 0

for v in tqdm(vocab):
    if v in glove:
        vec[v] = glove[v]
    else:
        oov_count += 1

print("OOV count=", oov_count, "total words=", len(vocab), "oov %=", oov_count/len(vocab))

vec["UNK"] = np.random.randn(300)
vec["<bos>"] = np.random.randn(300)
vec["<eos>"] = np.random.randn(300)

i2w = dict(enumerate(list(vec.keys())))
w2i = dict(map(reversed, i2w.items()))
pickle.dump(i2w, open(
    "/data/ramitra/marco/preprocessed/gmc/index2word.pkl", "wb"))

vectors = np.zeros((len(i2w), 300))
for idx, token in i2w.items():
    vectors[idx] = vec[token]

pickle.dump(vectors, open(
    "/data/ramitra/marco/preprocessed/gmc/vectors.pkl", "wb"))


max_a_len = 50
max_p_len = 200
model_vocab = set()
new_data = []


def set_unk(x):
    return [y if y in w2i else "UNK" for y in x]

for data in (train_data, dev_data):
    model_data = list()
    for x in tqdm(data):
        c = 0
        for i in range(len(x['ans'])):
            max_score = 0
            if not x['ans'][i] or len(x['ans'][i]) > max_a_len:
                continue
            for j in range(len(x['pos_p'])):
                span = find_span(x['ans'][i], x['pos_p'][j], 0.7)
                if len(span) == 3 and span[2] > max_score and len(x['pos_p'][j]) <= max_p_len:
                    max_score = span[2]
                    best_p = x['pos_p'][j]
            if max_score == 0:
                continue
            model_data.append(
                {"q": set_unk(x["query"]),
                 "p": set_unk(best_p),
                 "a": ["<bos>"] + set_unk(x["ans"][i]) + ["<eos>"],
                 "c": 1,
                 "l_p": len(best_p),
                 "l_q": len(x["query"]),
                 "l_a": len(x["ans"][i]) + 2
                }
            )
            c += 1
            
    new_data.append(model_data)

train_data = list(map(lambda x: {**{"q": [w2i[y] for y in x["q"]], "p": [w2i[y] for y in x["p"]],
                                    "a": [w2i[y] for y in x["a"]]},
                                 **{k: v for k, v in x.items() if k not in ("p", "q", "a")}}, new_data[0]))

dev_data = list(map(lambda x: {**{"q": [w2i[y] for y in x["q"]], "p": [w2i[y] for y in x["p"]],
                                  "a": [w2i[y] for y in x["a"]]},
                               **{k: v for k, v in x.items() if k not in ("p", "q", "a")}}, new_data[1]))

print(len(train_data), len(dev_data))
pickle.dump(train_data, open(
    "/data/ramitra/marco/preprocessed/gmc/train/data.pkl", "wb"))
pickle.dump(dev_data, open(
    "/data/ramitra/marco/preprocessed/gmc/valid/data.pkl", "wb"))
